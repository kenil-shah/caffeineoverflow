<?php

namespace App\Constants;

interface AnswerConstants
{
    const CREATE_RULES = [
        'body' => 'required'
    ];

    const UPDATE_RULES = [
        'body' => 'required',
    ];
}
